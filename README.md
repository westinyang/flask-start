![截图](./icon.png)
# flask-start

#### 介绍
Python Flask 快速开始，提供HTTP接口，Pyinstaller打包exe

#### 配置文件 `config.ini`

``` ini
[config]
; 服务端口号，默认5000
port=5000
```

#### 软件截图
![截图](./screenshot/1.png)

#### 安装依赖 / 运行

- `pip install configparser flask flask_cors`
- `python main.py`

#### 打包构建 / 清理

- `pip install pyinstaller`
- `build.bat`
- `clean.bat`

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request